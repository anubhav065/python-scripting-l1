# cook your code here
#for python 2.x
class ValueError(Exception):
    def __init__(self, value):
        self.value = value
        
def isListOfInts(list_):
	counter=1
	check=0
	if ord(list_[0]) !=91 and ord(list_[len(list_)-1]) != 93:
		raise ValueError(list_[0:len(list_)])
	else:
		for entry in list_[1:len(list_)]:
			if(counter==len(list_)-1):
				print list_, "--> True."
				break
			else:
				for element in entry:
					#list_ separated by comma, spaces or tabs, so in order to exclude these as an entry of list_
					if ord(element) == 44 or ord(element) == 32 or ord(element) == 9 or (ord(element)>=48 and ord(element)<=57):
						counter=counter+1
						continue
					elif ord(element)<48 or ord(element)>57:
						print list_, "--> False."
						counter=counter+1
						check=1
						break
				if(check==1):
					break
#using string approach:
list_=raw_input("enter a list")
try:
    isListOfInts(list_)
except ValueError as v:
    print "ValueError:", v.value, "is not of <list> type"