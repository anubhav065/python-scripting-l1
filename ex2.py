#for python 2.x
#save this .py file in the directory in which you have the file you want to read

import os
import sys

def isWhiteLine(line):

	st = []
	check = 'y'
	for word in line: #to check if there are only space, tab and newline character in the line
		if ord(word) != 32 and ord(word) != 9 and ord(word) != 10:
			check = 'n'
			break
		else:
			continue

	if check == 'y':
		return 'TRUE' #return TRUE if the line contains only blank characters
	elif check == 'n':
		for word in line:
			if ord(word) != 10:
				st.append(word)
			else:
				continue
		output=''.join(st)
		print output

file=open(os.path.abspath(sys.argv[1]),"r") 
for line in file:
	isWhiteLine(line) #pass a line from the file to function