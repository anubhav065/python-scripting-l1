#for python 2.x

import sys

def ruler(num):
	a = 1
	nos_ = 1
	for i in xrange(1,num+1,1):
	    if i%10 != 0:
	        sys.stdout.write(" ")
	    else:
	        sys.stdout.write(str(nos_))
	        nos_ = nos_ + 1
	
	print ""
	while num > 0:
		if a > 9:
			a = 0

		sys.stdout.write(str(a))
		a=a+1
		num=num-1

num=raw_input("enter a number") #takes input as a string
while(1):
	flag = 0
	for e in num:
		if ord(e) < 48 or ord(e) > 57: #to check if an entry is not in the range of 0-9.
			num=raw_input("enter a valid number")
			flag=1
			break
	if(flag == 0):
		num=int(num) #because initially input was in string format, this converts the digits to integer type.
		break

ruler(num)